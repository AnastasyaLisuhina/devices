import java.util.*;
import java.util.Scanner;

public class Main {

    private FileManager fm ;

    public static void main(String[] args) {
        new Main().run();
    }

    private void run(){
        fm = new  FileManager();
        java.util.Scanner scanner = new Scanner(System.in);

        while (true) {
            try {
                System.out.println("Enter command:");
                String line = scanner.nextLine();

                if (line.equals("exit")) {
                    System.exit(0);
                }

                if (line.equals("list")){
                    list();
                }
            }catch (DatabaseAccessException e) {
                System.out.println(e.getMessage());
            }
        }
    }
    private void list() throws DatabaseAccessException {
        HashMap<String, Device> data = fm.readData();
        for (Device device: data.values()){
            System.out.println(device.getDataString());
        }
    }
}

