import java.io.Serializable;

public class Printer extends Device implements Serializable {
    public boolean color;
    public boolean network;

    @Override
    protected String getSpecificDataString() {
        String color = this.color ? "colored": "black-and-white ";
        String network = this.network ? "network": "not network";
        return color + " " + network;
    }
}
