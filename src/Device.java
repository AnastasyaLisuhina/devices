import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public abstract class Device implements Serializable {

   public String sku;
   public String name;
   public Date startDate;

   protected abstract String getSpecificDataString();

   public String getDataString(){
      SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy");
      String date = df.format(startDate);
      return date + " " + name + " " + getSpecificDataString();
   }
}
