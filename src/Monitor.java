import java.io.Serializable;

public class Monitor extends Device implements Serializable {
    public int size;
    public boolean color;
    public Kind kind;

    @Override
    protected String getSpecificDataString() {
        String color = this.color ? "colored": "black-and-white ";
        String size = String.valueOf(this.size);
        return color + " " + kind.getValue() + " " + size;
    }

    public enum Kind {
       TUBE ("tube"),
       LCD ("lcd"),
       PROJECTOR ("projector");

        private String value;

        Kind (String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
   }
}
