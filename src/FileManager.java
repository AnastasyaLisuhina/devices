import java.io.*;
import java.util.HashMap;

public class FileManager {

    private static final String FILE_NAME = "data";

    public void writeData(HashMap<String, Device> data) throws DatabaseAccessException {
        try {
            FileOutputStream fos = new FileOutputStream(FILE_NAME);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(data);
            oos.close();
        } catch (IOException e) {
            throw new DatabaseAccessException();
        }
    }


    public HashMap<String, Device> readData() throws DatabaseAccessException {
        try {
            FileInputStream fin = new FileInputStream(FILE_NAME);
            ObjectInputStream ois = new ObjectInputStream(fin);
            HashMap<String, Device> data = (HashMap<String, Device>) ois.readObject();
            ois.close();
            return data;
        } catch (Exception e) {
            throw new DatabaseAccessException();
        }
    }

    public void deleteFile() throws DatabaseAccessException {
        try {
            File file = new File(FILE_NAME);
            file.delete();
        } catch (Exception e){
            throw new DatabaseAccessException();
        }
    }
}

