
public class DatabaseAccessException extends Exception {

    public DatabaseAccessException(){
        super("Can not access database");
    }
}